let programming = {
    languages: ["JavaScript", "Python", "Ruby"],
    isChallenging: true,
    isRewarding: true,
    difficulty: 8,
    jokes: "http://stackoverflow.com/questions/234075/what-is-your-best-programmer-joke"
};
//6.1. Viết lệnh để thêm key “Language” với giá trị là “Go” vào object trên
const newtext= {
    language : "go"
}  
// 6.2. Thay đổi difficulty từ 8 thành 7
const difficulty ={
    difficulty: 7
}

const AddObject=Object.assign(programming,newtext,difficulty)
console.log(AddObject);
// 6.2. Thay đổi difficulty từ 8 thành 7

// . In ra tất cả các giá trị của keys trong Object trên (Languages, isChallenging, …)
console.log( Object.values(AddObject));
//6.4. In ra tất cả các giá trị của values trong Object trên
console.log( Object.keys(AddObject));   